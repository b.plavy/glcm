function filterWH(originalImg,noisedImg,wSize)

% filterWH('Lena.bmp','LenaG20.bmp',3)

oImage = rgb2gray(imread(originalImg));
nImage = rgb2gray(imread(noisedImg));
imageMeta = imfinfo(noisedImg);
fImage = uint8(zeros(imageMeta.Width));

startFor=(wSize+1)/2;
endFor=startFor-1;
dimension=wSize^2;

R = single(zeros(dimension));
p = single(zeros(dimension,1));


for i = startFor:(imageMeta.Width)-endFor
    for j = startFor:(imageMeta.Height)-endFor
        xx = nImage(i-endFor:i+endFor,j-endFor:j+endFor,1);
        x = single(reshape(xx,1,[]));
        R = R + x'*x;
        p = p + single(oImage(i,j)).* x';
    end  
end
h = R\p
% sum(h)
for i = startFor:(imageMeta.Width)-endFor
    for j = startFor:(imageMeta.Height)-endFor
        xx = nImage(i-endFor:i+endFor,j-endFor:j+endFor,1);
        x = single(reshape(xx,1,[]));
        fImage(i,j) = uint8(x*h);
    end  
end
imshow(fImage)
end
% uloha upravit na adaptivnu filtraciu s aplikovanim na farebny obraz