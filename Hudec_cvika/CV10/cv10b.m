function varargout = cv10b(varargin)
% CV10B MATLAB code for cv10b.fig
%      CV10B, by itself, creates a new CV10B or raises the existing
%      singleton*.
%
%      H = CV10B returns the handle to a new CV10B or the handle to
%      the existing singleton*.
%
%      CV10B('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CV10B.M with the given input arguments.
%
%      CV10B('Property','Value',...) creates a new CV10B or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cv10b_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cv10b_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cv10b

% Last Modified by GUIDE v2.5 23-Nov-2022 14:24:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cv10b_OpeningFcn, ...
                   'gui_OutputFcn',  @cv10b_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cv10b is made visible.
function cv10b_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cv10b (see VARARGIN)

% Choose default command line output for cv10b
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cv10b wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cv10b_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function openVideo_Callback(hObject, eventdata, handles)
% hObject    handle to openVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fileName = uigetfile({'*.mp4'},'Select video file');
handles.video = VideoReader(fileName); guidata(hObject, handles);
axes(handles.inputVideo); imshow(readFrame(handles.video));
handles.actualFrame = 1; guidata(hObject, handles);


% --- Executes on button press in playButton.
function playButton_Callback(hObject, eventdata, handles)
% hObject    handle to playButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pauseDur = round(1/handles.video.FrameRate);
while(hasFrame(handles.video))
    imshow(readFrame(handles.video));
    handles.actualFrame = handles.actualFrame + 1; guidata(hObject, handles);
    pause(pauseDur);
end


% --- Executes on button press in nextFrame.
function nextFrame_Callback(hObject, eventdata, handles)
% hObject    handle to nextFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.inputVideo); imshow(readFrame(handles.video));
handles.actualFrame = handles.actualFrame + 1; guidata(hObject, handles);


% --- Executes on button press in previousFrame.
function previousFrame_Callback(hObject, eventdata, handles)
% hObject    handle to previousFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.actualFrame = handles.actualFrame - 1; guidata(hObject, handles);
axes(handles.inputVideo); imshow(read(handles.video, actualFrame -1));


% --- Executes on button press in lastFrame.
function lastFrame_Callback(hObject, eventdata, handles)
% hObject    handle to lastFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.inputVideo); imshow(read(handles.video,handles.video.NumFrames));
handles.actualFrame = handles.video.NumFrames; guidata(hObject, handles);

% --- Executes on button press in firstFrame.
function firstFrame_Callback(hObject, eventdata, handles)
% hObject    handle to firstFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.inputVideo); imshow(read(handles.video,1));
handles.actualFrame = 1; guidata(hObject, handles);


% --------------------------------------------------------------------
function executeFunction_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to executeFunction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
diffVector = zeros(1,handles.video.NumFrames);
for i=1:handles.video.NumFrames-1
    diffVector(1,i) = sum(sum(sum(imabsdiff(read(handles.video,i),read(handles.video,i+1)))));
end
axes(handles.diffVectorPlot); plot(diffVector);
