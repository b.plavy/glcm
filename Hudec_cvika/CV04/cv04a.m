function [O] = cv04a(inputImg)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

inputImage = imread(inputImg);
inputData = imfinfo(inputImg);

[riadky, stlpce, pocetKanalov] = size(inputImage);
tic
O = uint64(inputImage(:,:,1))*1 + uint64(inputImage(:,:,2))*10 + uint64(inputImage(:,:,3))*100;

    for i = 0:max(O(:));
        matrixOfPresence = O == i;
        hist(1,i+1) = sum(matrixOfPresence(:));
    end

bar(hist);
toc
end