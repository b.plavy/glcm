function cv04b(inputImg)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

inputImageRGB = imread(inputImg);

inputImageMetaData = imfinfo(inputImg);
inputImageHSV = rgb2hsv(inputImageRGB);

inputImageH = inputImageHSV(:,:,1);

tic
inputImageH2uint8 = uint8(inputImageH*255);

    for i = 0:max(inputImageH2uint8(:));
        matrixOfPresence = inputImageH2uint8 == i;
        hist(1,i+1) = sum(matrixOfPresence(:));
    end


figure(1);
subplot(2,3,1); imshow(inputImageRGB(:,:,1));
subplot(2,3,2); imshow(inputImageRGB(:,:,2));
subplot(2,3,3); imshow(inputImageRGB(:,:,3));

subplot(2,3,4); imshow(inputImageHSV(:,:,1));
subplot(2,3,5); imshow(inputImageHSV(:,:,2));
subplot(2,3,6); imshow(inputImageHSV(:,:,3));

figure(2);
stem(hist); 
toc
end