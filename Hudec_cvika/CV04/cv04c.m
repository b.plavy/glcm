function cv04c(inputImg1, inputImg2)
%UNTITLED4 Summary of this function goes here
%  Detailed explanation goes here
% cv04c('Cloud_01.bmp','Cloud_02.bmp')

inputImageRGB1 = imread(inputImg1);
inputImageRGB2 = imread(inputImg2);

hist1 = zeros(1,256);
hist2 = zeros(1,256);

inputImageMetaData1 = imfinfo(inputImg1);
inputImageMetaData2 = imfinfo(inputImg2);

hist1 = cv04cx(inputImageRGB1);
hist2 = cv04cx(inputImageRGB2);

subplot(2,2,1); imshow(inputImageRGB1);
subplot(2,2,2); imshow(inputImageRGB2);
subplot(2,2,3); stem(hist1);
subplot(2,2,4); stem(hist2);

difference = sqrt(sum((hist1 - hist2).^2))
corrcoef(hist1, hist2)

end