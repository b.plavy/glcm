function [hist] = cv04cx(inputImageRGB)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


inputImageHSV = rgb2hsv(inputImageRGB);

inputImageH = inputImageHSV(:,:,1);
hist=zeros(1,256);
inputImageH2uint8 = uint8(inputImageH*255);

    for i = 0:max(inputImageH2uint8(:));
        matrixOfPresence = inputImageH2uint8 == i;
        hist(1,i+1) = sum(matrixOfPresence(:));
    end

end