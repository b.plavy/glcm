function [] = cv03b(inputImg, noOfBits)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

inputImage = imread(inputImg);

divide=2^noOfBits;
gray = rgb2gray(inputImage);
graySmall = imresize(gray, 0.15);
A=round(gray/(256/divide));
outputImg = A*(256/divide);

for i=0:255
    matrixOfPresence = gray==i;
    hist(1, i+1) = sum(matrixOfPresence(:));

    matrixOfPresenceSmall = graySmall==i;
    histSmall(1, i+1) = sum(matrixOfPresenceSmall(:));
%     figure(2); imshow(matrixOfPresence, [0 1]); pause;
end
hold on;
subplot(2,2,1); imshow(gray);
subplot(2,2,2); plot(hist, 'r');
subplot(2,2,3); imshow(graySmall);
subplot(2,2,4); plot(histSmall, 'g');
end
