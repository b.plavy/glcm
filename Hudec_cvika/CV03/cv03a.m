function [outputArg1,outputArg2] = cv03a(inputImg, noOfBits)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

inputImage = imread(inputImg);

divide=2^noOfBits;
gray = rgb2gray(inputImage);
A=round(gray/(256/divide));
outputImg = A*(256/divide);

subplot(1,2,1); imshow(gray);
subplot(1,2,2); imshow(outputImg);

end