function [] = cv03c(inputImg, noOfBits)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

inputImage = imread(inputImg);

divide=2^noOfBits;
gray = rgb2gray(inputImage);
grayReduced=round(gray/(256/divide));

outputImgReduced = grayReduced*(256/divide);

for i=0:255
    matrixOfPresence = gray==i;
    hist(1, i+1) = sum(matrixOfPresence(:));

    matrixOfPresenceReduced = outputImgReduced==i;
    histReduced(1, i+1) = sum(matrixOfPresenceReduced(:));
%     figure(2); imshow(matrixOfPresence, [0 1]); pause;
end
hold on;
subplot(2,2,1); imshow(gray);
subplot(2,2,2); plot(hist, 'r');
subplot(2,2,3); imshow(outputImgReduced);
subplot(2,2,4); plot(histReduced, 'g');
end
