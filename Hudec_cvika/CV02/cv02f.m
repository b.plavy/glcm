function [outputImg] = cv02f(inputImg)
close all;
% [imageLena] = cv02f('Lena.png')

inputImage=imread(inputImg);
%imshow(inputImage);
imgMetadata = imfinfo(inputImg);

channelR = inputImage(:,:,1); energyR = sum(channelR(:));
channelG = inputImage(:,:,2); energyG = sum(channelG(:));
channelB = inputImage(:,:,3); energyB = sum(channelB(:));
[energyR energyG energyB]

outputImg(:,:,1) = channelG;
outputImg(:,:,2) = channelB;
outputImg(:,:,3) = channelR;

subplot(1,2,1); imshow(inputImg);
subplot(1,2,2); imshow(outputImg);

[rb] = corr2(channelR,channelB)
[gb] = corr2(channelG,channelB)
[rg] = corr2(channelR,channelG)


corrMatrix = [1 , rg, rb; rg, 1, gb; rb, gb, 1]

end