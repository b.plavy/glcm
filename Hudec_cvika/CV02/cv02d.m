function [imageLena] = cv02d(inputImg, order)
close all;
% [imageLena] = cv02d('Lena.png',[1 2 3 4])

inputImage=imread(inputImg);
%imshow(inputImage);

imgMetadata = imfinfo(inputImg);

k1 = inputImage(1:128,1:128,:);
k2 = inputImage(1:128,129:256,:);
k3 = inputImage(129:256,1:128,:);
k4 = inputImage(129:256,129:256,:);

% position1 = order(1);
% position2 = order(2);
% position3 = order(3);
% position4 = order(4);
% 
% if position1 == 1
%     a1=k1;
% elseif position1 == 2
%     a1=k2;
% elseif position1 == 3
%     a1=k3;
% elseif position1 == 4
%     a1=k4;
% end
% 
% 
% if position2 == 1
%     a2=k1;
% elseif position2 == 2
%     a2=k2;
% elseif position2 == 3
%     a2=k3;
% elseif position2 == 4
%     a2=k4;
% end
% 
% if position3 == 1
%     a3=k1;
% elseif position3 == 2
%     a3=k2;
% elseif position3 == 3
%     a3=k3;
% elseif position3 == 4
%     a3=k4;
% end
% 
% 
% if position4 == 1
%     a4=k1;
% elseif position4 == 2
%     a4=k2;
% elseif position4 == 3
%     a4=k3;
% elseif position4 == 4
%     a4=k4;
% end

a1=

figure(1);
subplot(2,2,1); imshow(a1);
subplot(2,2,2); imshow(a2);
subplot(2,2,3); imshow(a3);
subplot(2,2,4); imshow(a4);

figure(2);
imageLena = [order(1),a1;a4,a3];
imshow(imageLena)

imageLena=inputImage;
end