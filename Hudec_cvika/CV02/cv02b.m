function [imageLena] = cv02b(inputImg)

% [imageLena] = cv02b('Lena.png')

inputImage=imread(inputImg);
%imshow(inputImage);


imgMetadata = imfinfo(inputImg);

k1 = inputImage(1:128,1:128,:);
k2 = inputImage(1:128,129:256,:);
k3 = inputImage(129:256,1:128,:);
k4 = inputImage(129:256,129:256,:);

figure(1);
subplot(2,2,1); imshow(k1);
subplot(2,2,2); imshow(k2);
subplot(2,2,3); imshow(k3);
subplot(2,2,4); imshow(k4);

figure(2);
imageLena = [k2,k1;k4,k3];
imshow(imageLena)

imageLena=inputImage;
end