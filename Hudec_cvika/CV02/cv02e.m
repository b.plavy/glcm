function [imageLena] = cv02e(inputImg)
close all;
% [imageLena] = cv02e('Lena.png')

inputImage=imread(inputImg);
%imshow(inputImage);
imgMetadata = imfinfo(inputImg);

% zeroMatrix = zeros(imgMetadata.Width, imgMetadata.Height)

imgR = uint8(zeros(imgMetadata.Width, imgMetadata.Height,3));
imgG = uint8(zeros(imgMetadata.Width, imgMetadata.Height,3));
imgB = uint8(zeros(imgMetadata.Width, imgMetadata.Height,3));

imgR(:,:,1) = inputImage(:,:,1);
imgG(:,:,2) = inputImage(:,:,2);
imgB(:,:,3) = inputImage(:,:,3);

subplot(1,3,1); imshow(imgR);
subplot(1,3,2); imshow(imgG);
subplot(1,3,3); imshow(imgB);

imageLena = [inputImg];

end