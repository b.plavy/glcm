function varargout = ZapocetOprava(varargin)
% ZAPOCETOPRAVA MATLAB code for ZapocetOprava.fig
%      ZAPOCETOPRAVA, by itself, creates a new ZAPOCETOPRAVA or raises the existing
%      singleton*.
%
%      H = ZAPOCETOPRAVA returns the handle to a new ZAPOCETOPRAVA or the handle to
%      the existing singleton*.
%
%      ZAPOCETOPRAVA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZAPOCETOPRAVA.M with the given input arguments.
%
%      ZAPOCETOPRAVA('Property','Value',...) creates a new ZAPOCETOPRAVA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ZapocetOprava_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ZapocetOprava_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ZapocetOprava

% Last Modified by GUIDE v2.5 14-Dec-2022 13:37:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ZapocetOprava_OpeningFcn, ...
                   'gui_OutputFcn',  @ZapocetOprava_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ZapocetOprava is made visible.
function ZapocetOprava_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ZapocetOprava (see VARARGIN)

% Choose default command line output for ZapocetOprava
handles.output = hObject;

imshow(ones(1), 'parent', handles.inputImage);
imshow(ones(1), 'parent', handles.outputImage);
imshow(ones(1), 'parent', handles.diffImage);
set(handles.resolutionValue, 'String', '');
set(handles.filenameValue, 'String', '');
set(handles.differenceValue, 'String', '');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ZapocetOprava wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ZapocetOprava_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in methodSelect.
function methodSelect_Callback(hObject, eventdata, handles)
% hObject    handle to methodSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns methodSelect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from methodSelect


% --- Executes during object creation, after setting all properties.
function methodSelect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to methodSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in previousButton.
function previousButton_Callback(hObject, eventdata, handles)
% hObject    handle to previousButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.current > 1)
    handles.current = handles.current - 1;
end

guidata(hObject,handles);

drawCurrent(hObject, eventdata, handles);

% --- Executes on button press in nextButton.
function nextButton_Callback(hObject, eventdata, handles)
% hObject    handle to nextButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.current < length(handles.Data))
    handles.current = handles.current + 1;
end

drawCurrent(hObject, eventdata, handles);

% --------------------------------------------------------------------
function menu_Callback(hObject, eventdata, handles)
% hObject    handle to menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
path = uigetdir();
files = dir(fullfile(path, '*.JPG'));
handles.current = 1;

handles.Data(1:length(files)) = struct('Image', [], 'Name', '', 'Resolution', '');
for j = 1:length(files)
  handles.Data(j).Image = imread(strcat(path,"\",files(j).name));
  handles.Data(j).Name = files(j).name;
  info = imfinfo(strcat(path,"\",files(j).name));
  handles.Data(j).Resolution = strcat(num2str(info.Width),'x',num2str(info.Width));
end
guidata(hObject, handles);
drawCurrent(hObject, eventdata, handles);

function drawCurrent(hObject, eventdata, handles)
imshow(handles.Data(handles.current).Image, 'parent', handles.inputImage);
set(handles.resolutionValue, 'String', handles.Data(handles.current).Resolution);
set(handles.filenameValue, 'String', handles.Data(handles.current).Name);
imshow(ones(1), 'parent', handles.outputImage);
imshow(ones(1), 'parent', handles.diffImage);
set(handles.differenceValue, 'String', '');
guidata(hObject, handles);    

% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close();

% --------------------------------------------------------------------
function applyButton_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to applyButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.methodSelect, 'Value');
sliderVal = get(handles.slider1, 'Value');
im = handles.Data(handles.current).Image;
if(val == 1)
    im_hsv = rgb2hsv(im);

    h = im_hsv(:,:,1);
    s = im_hsv(:,:,2);
    v = im_hsv(:,:,3);

    gr = h >= (60-sliderVal)/360 & h <= (60+sliderVal)/360;

    v(gr) = 0;
    im_hsv_mod = cat(3, h, s, v);
    handles.output = hsv2rgb(im_hsv_mod);
else

    min = get(handles.slider1, 'Min');
    max = get(handles.slider1, 'Max');
    
    numColors = 3;
    L = imsegkmeans(im,numColors);
    B = labeloverlay(im,L);

    lab_he = rgb2lab(im);
    ab = lab_he(:,:,2:3);
    ab = im2single(ab);
    pixel_labels = imsegkmeans(ab,numColors,NumAttempts=3);

    mask1 = pixel_labels == 1;
    mask2 = pixel_labels == 2;
    mask3 = pixel_labels == 3;
    
    if(sliderVal<max/3)
        handles.output = im.*uint8(mask1);
    elseif(sliderVal<2*max/3)
        handles.output = im.*uint8(mask2);
    else
        handles.output = im.*uint8(mask3);
    end

end

handles.differenceImage = im2double(im) - im2double(handles.output);

imshow(handles.output, 'parent', handles.outputImage);
imshow(handles.differenceImage, 'parent', handles.diffImage);
set(handles.differenceValue, 'String', sum(sum(sum(handles.differenceImage))));

guidata(hObject, handles);
