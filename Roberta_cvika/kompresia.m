% kompresia obrazka - 8bit maximum
function kompresia(src, bit)
    img = imread(src);

    img_output = round(img./(256/(2^bit)));
    img_output = img_output.*(256/(2^bit));

    subplot(1,2,1);
    imshow(img);
    subplot(1,2,2);
    imshow(img_output);
end

