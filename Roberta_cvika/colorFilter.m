% color filter
function colorFilter (rgb)
    img = imread('./Lena.bmp');
    showRGB = uint8(zeros(size(img)));

    if contains(rgb, "R")
        showRGB(:, :, 1) = img(:, : ,1);
    end
    if contains(rgb, "G")
        showRGB(:, :, 2) = img(:, : ,2);
    end
    if contains(rgb, "B")
        showRGB(:, :, 3) = img(:, : ,3);
    end
    
    imshow(showRGB);
end

