%shaar, sobel, perwitt etc.

function varargout = hranove_detektory(varargin)
% HRANOVE_DETEKTORY MATLAB code for hranove_detektory.fig
%      HRANOVE_DETEKTORY, by itself, creates a new HRANOVE_DETEKTORY or raises the existing
%      singleton*.
%
%      H = HRANOVE_DETEKTORY returns the handle to a new HRANOVE_DETEKTORY or the handle to
%      the existing singleton*.
%
%      HRANOVE_DETEKTORY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HRANOVE_DETEKTORY.M with the given input arguments.
%
%      HRANOVE_DETEKTORY('Property','Value',...) creates a new HRANOVE_DETEKTORY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before hranove_detektory_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to hranove_detektory_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help hranove_detektory

% Last Modified by GUIDE v2.5 22-Nov-2022 12:38:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @hranove_detektory_OpeningFcn, ...
                   'gui_OutputFcn',  @hranove_detektory_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before hranove_detektory is made visible.
function hranove_detektory_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to hranove_detektory (see VARARGIN)

% Choose default command line output for hranove_detektory
handles.output = hObject;

handles.shaarX = [-3 0 3; -10 0 10; -3 0 3];
handles.shaarY = handles.shaarX';
handles.sobelX = [-1 0 1; -2 0 2; -1 0 1];
handles.sobelY = handles.sobelX';
handles.prewittX1 = [-1 0 1; -1 0 1; -1 0 1];
handles.prewittX2 = [1 0 -1; 1 0 -1; 1 0 -1];
handles.prewittY = [-1 -1 -1; 0 0 0; 1 1 1];

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes hranove_detektory wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = hranove_detektory_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadImage.
function loadImage_Callback(hObject, eventdata, handles)
% hObject    handle to loadImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile({'*.png'; '*.bmp'; '*.jpg'}, 'Select image');

image = imread(strcat(pathname, filename));
handles.image = image;
grayImg = rgb2gray(image);
handles.grayImg = grayImg;
    
axes(handles.imageArea);
imshow(image);

guidata(hObject, handles);


% --- Executes on selection change in edges.
function edges_Callback(hObject, eventdata, handles)
% hObject    handle to edges (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns edges contents as cell array
%        contents{get(hObject,'Value')} returns selected item from edges
gray = double(handles.grayImg);

switch get(handles.edges, 'Value')
    case 1
        img = conv2(gray, handles.shaarX);
    case 2
        img = conv2(gray, handles.shaarY);
    case 3
        img = conv2(gray, handles.sobelX);
    case 4
        img = conv2(gray, handles.sobelY);
    case 5
        img = conv2(gray, handles.prewittX1);
    case 6
        img = conv2(gray, handles.prewittX2);
    case 7
        img = conv2(gray, handles.prewittY);
end

imshow(img);


% --- Executes during object creation, after setting all properties.
function edges_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edges (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
