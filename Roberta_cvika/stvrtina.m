function stvrtina(vstup)
    img = imread(vstup);
    %imshow(img);

    %image info
    imgInfo = imfinfo(vstup)

    %sirka a vyska
    sirka = imgInfo.Width;
    vyska = imgInfo.Height;

    %image size v jednej premennej
    imgSize = [sirka, vyska]

    %stvrtina obrazku
    imshow(img(1:imgSize(1, 1)/2, 1:imgSize(1, 2)/2, :))

    %viacero obrazkov zaroven
    subplot(2, 1, 1)
    imshow(img)

    subplot(2,1,2)
    imshow(img(1:sirka/2, 1:vyska/2, :))
    
end