% Hranove detekcie (roberts, canny, sharr and shit) 
% Brightness
% Gray and binary
% Frequence

function varargout = cv11(varargin)
% CV11 MATLAB code for cv11.fig
%      CV11, by itself, creates a new CV11 or raises the existing
%      singleton*.
%
%      H = CV11 returns the handle to a new CV11 or the handle to
%      the existing singleton*.
%
%      CV11('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CV11.M with the given input arguments.
%
%      CV11('Property','Value',...) creates a new CV11 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cv11_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cv11_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cv11

% Last Modified by GUIDE v2.5 29-Nov-2022 13:08:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cv11_OpeningFcn, ...
                   'gui_OutputFcn',  @cv11_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cv11 is made visible.
function cv11_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cv11 (see VARARGIN)

% Choose default command line output for cv11
handles.output = hObject;


handles.shaarX = [-3 0 3; -10 0 10; -3 0 3];
handles.shaarY = handles.shaarX';
handles.sobelX = [-1 0 1; -2 0 2; -1 0 1];
handles.sobelY = handles.sobelX';
handles.prewittX1 = [-1 0 1; -1 0 1; -1 0 1];
handles.prewittX2 = [1 0 -1; 1 0 -1; 1 0 -1];
handles.prewittY = [-1 -1 -1; 0 0 0; 1 1 1];

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cv11 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cv11_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadBtn.
function loadBtn_Callback(hObject, eventdata, handles)
% hObject    handle to loadBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile({'*.png'; '*.bmp'; '*.jpg'}, 'Select image');

image = imread(strcat(pathname, filename));
handles.image = image;
grayImg = rgb2gray(image);
handles.grayImg = grayImg;
    
axes(handles.imageArea);
imshow(image);

guidata(hObject, handles);


% --- Executes on selection change in imageSwitcher.
function imageSwitcher_Callback(hObject, eventdata, handles)
% hObject    handle to imageSwitcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns imageSwitcher contents as cell array
%        contents{get(hObject,'Value')} returns selected item from imageSwitcher
axes(handles.imageArea2);
bnw = im2bw(handles.grayImg);
handles.bnw = bnw;

switch get(handles.imageSwitcher, 'Value')
    case 1
        img = handles.grayImg;
    case 2
        img = bnw;
end

imshow(img);

guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function imageSwitcher_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageSwitcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in edgeDetection.
function edgeDetection_Callback(hObject, eventdata, handles)
% hObject    handle to edgeDetection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns edgeDetection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from edgeDetection

axes(handles.imageArea3);
gray = double(handles.grayImg);

switch get(handles.edgeDetection, 'Value')
    case 1
        img = edge(handles.bnw, 'roberts', 0.5);
    case 2
        img = edge(handles.bnw, 'canny', 0.4);
    case 3
        img = conv2(gray, handles.shaarX);
    case 4
        img = conv2(gray, handles.shaarY);
    case 5
        img = conv2(gray, handles.sobelX);
    case 6
        img = conv2(gray, handles.sobelY);
    case 7
        img = conv2(gray, handles.prewittX1);
    case 8
        img = conv2(gray, handles.prewittX2);
    case 9
        img = conv2(gray, handles.prewittY);
end

imshow(img);

guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function edgeDetection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edgeDetection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function brightnessSlider_Callback(hObject, eventdata, handles)
% hObject    handle to brightnessSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
brightnessSliderValue = get(handles.brightnessSlider, 'Value');

axes(handles.imageArea);
bImage = im2double(handles.image);
img2 = bImage + brightnessSliderValue;
imshow(img2);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function brightnessSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to brightnessSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in freqSwitcher.
function freqSwitcher_Callback(hObject, eventdata, handles)
% hObject    handle to freqSwitcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imgDCT = dct2(im2double(handles.grayImg));
handles.imgDCT = imgDCT;
axes(handles.imageArea4);
imshow(imgDCT);

guidata(hObject, handles);



function freqValueSwitcher_Callback(hObject, eventdata, handles)
% hObject    handle to freqValueSwitcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freqValueSwitcher as text
%        str2double(get(hObject,'String')) returns contents of freqValueSwitcher as a double
freqValue = str2num(get(handles.freqValueSwitcher, 'String'));

zig = zigzag(handles.imgDCT);
zig(freqValue:end) = 0;
a = izigzag(zig, size(handles.image, 1), size(handles.image, 1));
idct = idct2(a);

axes(handles.imageArea5);
imshow(idct);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function freqValueSwitcher_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freqValueSwitcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
