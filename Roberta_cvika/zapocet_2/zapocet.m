function varargout = zapocet(varargin)
% ZAPOCET MATLAB code for zapocet.fig
%      ZAPOCET, by itself, creates a new ZAPOCET or raises the existing
%      singleton*.
%
%      H = ZAPOCET returns the handle to a new ZAPOCET or the handle to
%      the existing singleton*.
%
%      ZAPOCET('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZAPOCET.M with the given input arguments.
%
%      ZAPOCET('Property','Value',...) creates a new ZAPOCET or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before zapocet_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to zapocet_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help zapocet

% Last Modified by GUIDE v2.5 06-Dec-2022 12:00:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @zapocet_OpeningFcn, ...
                   'gui_OutputFcn',  @zapocet_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before zapocet is made visible.
function zapocet_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to zapocet (see VARARGIN)

% Choose default command line output for zapocet
handles.output = hObject;

handles.shaarX = [-3 0 3; -10 0 10; -3 0 3];
handles.shaarY = handles.shaarX';
handles.sobelX = [-1 0 1; -2 0 2; -1 0 1];
handles.sobelY = handles.sobelX';
handles.prewittX1 = [-1 0 1; -1 0 1; -1 0 1];
handles.prewittX2 = [1 0 -1; 1 0 -1; 1 0 -1];
handles.prewittY = [-1 -1 -1; 0 0 0; 1 1 1];

handles.noiseChanger.Enable = 'off';
handles.frequenceChanger.Enable = 'off';
handles.edgeDetectorChanger.Enable = 'off';
handles.operatorChanger.Enable = 'off';
handles.edgeSlider.Enable = 'off';

handles.distanceInput.Enable = 'off';
handles.angleInput.Enable = 'off';
handles.generateGLCBtn.Enable = 'off';

handles.removeBG.Enable = 'on';

handles.edge = 'Canny';
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes zapocet wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = zapocet_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function fileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function aboutMenu_Callback(hObject, eventdata, handles)
% hObject    handle to aboutMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function exitMenu_Callback(hObject, eventdata, handles)
% hObject    handle to exitMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Are u sure?', 'Question', 'Yes', 'No', 'No');

if answer == "Yes"
    close();
end


% --------------------------------------------------------------------
function aboutMenuBtn_Callback(hObject, eventdata, handles)
% hObject    handle to aboutMenuBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ja = 'Tento super neskutocny matlabovsky skript napisal Branko';
msgbox(ja);


% --------------------------------------------------------------------
function loadMenu_Callback(hObject, eventdata, handles)
% hObject    handle to loadMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile({'*.png'; '*.bmp'; '*.jpg'}, 'Select image');

image = imread(strcat(pathname, filename));
handles.image = image;

grayImg = rgb2gray(image);
handles.grayImg = grayImg;

handles.noiseChanger.Enable = 'on';

axes(handles.imageArea);
imshow(image);

guidata(hObject, handles);


% --------------------------------------------------------------------
function saveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to saveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName] = uiputfile({'*.png'; '*.bmp'; '*.jpg'});
if isequal(FileName,0) || isequal(PathName,0)
    disp('User Clicked Cancel.')
else
    disp('Saved!');
    saveas(gcf, FileName);
    close(gcf);
end


% --- Executes on selection change in noiseChanger.
function noiseChanger_Callback(hObject, eventdata, handles)
% hObject    handle to noiseChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns noiseChanger contents as cell array
%        contents{get(hObject,'Value')} returns selected item from noiseChanger
noisedImage = zeros(size(handles.grayImg));
handles.noisedImage = noisedImage;

axes(handles.noiseArea);

switch (get(handles.noiseChanger, 'Value'))
    case 1
        noisedImage = imnoise(handles.grayImg, 'salt & pepper', 0.05);
    case 2
        doubleImage = im2double(rgb2gray(handles.image));
        p = 0.2;
        noisedImage = (doubleImage + p * rand(size(doubleImage))) / (1 + p);
end

handles.frequenceChanger.Enable = 'on';

 
imshow(noisedImage);


% --- Executes during object creation, after setting all properties.
function noiseChanger_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noiseChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function frequenceChanger_Callback(hObject, eventdata, handles)
% hObject    handle to frequenceChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of frequenceChanger as text
%        str2double(get(hObject,'String')) returns contents of frequenceChanger as a double
freqValue = str2num(get(handles.frequenceChanger, 'String'));
imgDCT = dct2(im2double(handles.grayImg));
handles.imgDCT = imgDCT;

zig = zigzag(imgDCT);
zig(freqValue:end) = 0;
a = izigzag(zig, size(handles.image, 1), size(handles.image, 1));
idct = idct2(a);

handles.edgeDetectorChanger.Enable = 'on';
handles.operatorChanger.Enable = 'on';

axes(handles.filterArea);
imshow(idct);

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function frequenceChanger_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frequenceChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in edgeDetectorChanger.
function edgeDetectorChanger_Callback(hObject, eventdata, handles)
% hObject    handle to edgeDetectorChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns edgeDetectorChanger contents as cell array
%        contents{get(hObject,'Value')} returns selected item from edgeDetectorChanger
axes(handles.edgeArea);

switch(get(handles.edgeDetectorChanger, 'Value'))
    case 1
        handles.robertsSlider.Enable = 'on';
        imageRoberts = edge(handles.grayImg, 'roberts', get(handles.edgeSlider, 'Value'));
        handles.imageRoberts = imageRoberts;
        imshow(handles.imageRoberts);
    case 2
        handles.cannySlider.Enable = 'on';

        imageCanny = edge(handles.grayImg, 'canny', get(handles.edgeSlider, 'Value'));
        handles.imageCanny = imageCanny;
        imshow(handles.imageCanny); 
end
handles.edgeSlider.Enable = 'on';
handles.distanceInput.Enable = 'on';

guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function edgeDetectorChanger_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edgeDetectorChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in operatorChanger.
function operatorChanger_Callback(hObject, eventdata, handles)
% hObject    handle to operatorChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns operatorChanger contents as cell array
%        contents{get(hObject,'Value')} returns selected item from operatorChanger
gray = double(handles.grayImg);
axes(handles.edgeArea);

switch get(handles.operatorChanger, 'Value')
    case 1
        img = conv2(gray, handles.shaarX);
    case 2
        img = conv2(gray, handles.shaarY);
    case 3
        img = conv2(gray, handles.sobelX);
    case 4
        img = conv2(gray, handles.sobelY);
    case 5
        img = conv2(gray, handles.prewittX1);
    case 6
        img = conv2(gray, handles.prewittX2);
    case 7
        img = conv2(gray, handles.prewittY);
end

handles.edgeSlider.Enable = 'off';
handles.distanceInput.Enable = 'on';

imshow(img);

% --- Executes during object creation, after setting all properties.
function operatorChanger_CreateFcn(hObject, eventdata, handles)
% hObject    handle to operatorChanger (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function angleInput_Callback(hObject, eventdata, handles)
% hObject    handle to angleInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of angleInput as text
%        str2double(get(hObject,'String')) returns contents of angleInput as a double
handles.generateGLCBtn.Enable = 'on';


% --- Executes during object creation, after setting all properties.
function angleInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angleInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function distanceInput_Callback(hObject, eventdata, handles)
% hObject    handle to distanceInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of distanceInput as text
%        str2double(get(hObject,'String')) returns contents of distanceInput as a double
handles.angleInput.Enable = 'on';


% --- Executes during object creation, after setting all properties.
function distanceInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to distanceInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in generateGLCBtn.
function generateGLCBtn_Callback(hObject, eventdata, handles)
% hObject    handle to generateGLCBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.GLCArea);
distance = str2num(get(handles.distanceInput, 'String'));
angle = str2num(get(handles.angleInput, 'String'));
matica = graycomatrix(handles.grayImg, 'Offset', [distance angle], 'NumLevels',256);
imshow(matica);

contrastValue = graycoprops(matica, 'Contrast');
handles.contrast = contrastValue.Contrast();
set(handles.contrastValue, 'String', handles.contrast);

homoValue = graycoprops(matica, 'Homogeneity');
handles.homo = homoValue.Homogeneity();
set(handles.homogeneityValue, 'String', handles.homo);

handles.removeBG.Enable = 'on';

guidata(hObject, handles);


% --- Executes on button press in removeBG.
function removeBG_Callback(hObject, eventdata, handles)
% hObject    handle to removeBG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.backgroundlessArea);
img = handles.image;
gray = handles.grayImg;
SE  = strel('Disk',1,4);
morphologicalGradient = imsubtract(imdilate(gray, SE),imerode(gray, SE));
mask = im2bw(morphologicalGradient,0.03);
SE  = strel('Disk',3,4);
mask = imclose(mask, SE);
mask = imfill(mask,'holes');
mask = bwareafilt(mask,1);
notMask = ~mask;
mask = mask | bwpropfilt(notMask,'Area',[-Inf, 5000 - eps(5000)]);
showMaskAsOverlay(0.5,mask,'r');
h = impoly(imgca,'closed',false);
fcn = makeConstrainToRectFcn('impoly',get(imgca,'XLim'),get(imgca,'YLim'));
drawfreehand(h,fcn);
gray = rgb2gray(img);
gray(~mask) = 255;
r = img(:,:,1);
g = img(:,:,2);
b = img(:,:,3);
r(~mask) = 255;
g(~mask) = 255;
b(~mask) = 255;
img = cat(3,r,g,b);
imshow(img);


% --- Executes on slider movement.
function edgeSlider_Callback(hObject, eventdata, handles)
% hObject    handle to edgeSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
edgeDetectorChanger_Callback(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function edgeSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edgeSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
