% Suffle regions of interest by number of rows with compression

function shuffle_roi (im, rows, bit)

% Load image and get dimensions
img = imread(im);
info = imfinfo(im);
height = info.Height();
width  = info.Width();

% Random permutation for suffle
r = randperm(rows);

heightSize = height/rows;
widthSize = width/rows;

% Compression
img_output = round(img./(256/(2^bit)));
img_output = img_output.*(256/(2^bit));

% Looping through all parts of compressed image and suffle
for i=1:rows
  for j=1:rows
    z(heightSize*(i-1)+1:heightSize*i,widthSize*(j-1)+1:widthSize*j, :) = img_output(heightSize*(r(i)-1)+1:heightSize*r(i),widthSize*(r(j)-1)+1:widthSize*r(j),:);
  end
end
imshow(z);
end

