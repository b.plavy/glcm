% random rows with kompression v1
function randomRows(input, rows, bit)
    img = imread(input);

    info = imfinfo(input);
    height = info.Height();
    width = info.Width();

    heightSize = height/rows;

    z = zeros(width, height, 3);

    r = randperm(rows);

    img_output = round(img./(256/(2^bit)));
    img_output = img_output.*(256/(2^bit));

    for i=1:rows
        j = r(i);
        z(heightSize*(i-1)+1:heightSize*(i-1)+heightSize, : , :) = img_output(heightSize*(j-1)+1:heightSize*(j-1)+heightSize, : , :);
    end

    imshow(uint8(z))

end

% random rows with kompression v2
% function randomRows(input, columns)
%     img = imread(input);
% 
%     info = imfinfo(input);
%     height = info.Height();
%     width = info.Width();
% 
%     heightSize = height/columns;
% 
%     randMatrix = uint8(zeros(heightSize, width, 3));
% 
%     while 1
%         randNum = randperm(columns);
%         if randNum == [1,2,3,4]
%             break;
%         end
%     end
% 
%     for i=0:(columns-1)
%         randMatrix(((i*heightSize)+1):((i+1)*heightSize), :, :) = img(((randNum(i+1)-1)*heightSize+1):((randNum(i+1)*heightSize)), :, :);
%     end
% 
%     figure;
%     subplot(1,2,1);
%     imshow(img);
%     subplot(1,2,2);
%     imshow(randMatrix)
% end