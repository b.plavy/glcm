function delenieAMedian()
    img = imread('./Lena.bmp');
    dim = size(img);
    s = 1;
    k = 4;

    dupl = img;

    for i = 1:4      
        for j = 1:4
            subplot(k, k, s)
            x1Pos = (i-1)*(dim(1)/4)+1;
            x2Pos = i*(dim(1)/4);
            y1Pos = (j-1)*(dim(1)/4)+1;
            y2Pos = j*(dim(1)/4);
            imshow(img(x1Pos:x2Pos, y1Pos:y2Pos ,:))

            s = s +1;
        end
    end

%     for i = 1:4      
%         for j = 1:4
%             x1Pos = (i-1)*(dim(1)/4)+1;
%             x2Pos = i*(dim(1)/4);
%             y1Pos = (j-1)*(dim(1)/4)+1;
%             y2Pos = j*(dim(1)/4);
%             for l = 1:3
%                 dupl(x1Pos:x2Pos, y1Pos:y2Pos, l) = medfilt2(img(x1Pos:x2Pos, y1Pos:y2Pos ,l));
%             end
%             s = s +1;
%         end
%     end
% 
%     imshow(dupl)
end

