% DCT, IDCT, ZIGZAG
function DCT_spect_filt(img, bit, rows, columns, value)
%     Load and get dimensions from image
    image = imread(img);
    info = imfinfo(img);
    h = info.Height();
    w = info.Width();

%     conversion to grey
    imgGrey = rgb2gray(image);

    heightSize = h/columns;
    widthSize = w/rows;

%     s cuz of subplots
    s = 1;

%     zero matrixes
    z = uint8(zeros(h, w, 3));
    newImg = uint8(zeros(h, w));

%     looping through rows and columns
    for i=1:rows
        for j=1:columns
%             fill z matrix with parts of image
            z = imgGrey(heightSize*(i-1)+1:heightSize*i, widthSize*(j-1)+1:widthSize*j, :);

%             creating DCT2 matrix
            dctImg = dct2(z);

%             zigzag method on DCT matrix
            zz = zigzag(dctImg);
            s = s + 1;
            
%             setting 0 value to some of the bits by input parameter
            zz(value:length(zz)) = 0;

%             inverse zigzag
            izz = izigzag(zz, heightSize, widthSize);

%             inverse DCT matrix
            iDctImg = idct2(izz);

%             filling parts of zero matrix with iDCT values
            newImg(heightSize*(i-1)+1:heightSize*i, widthSize*(j-1)+1:widthSize*j, :) = iDctImg;
        end
    end

%     create subdivided image
    subdivided = imgGrey - newImg;
    
%     show normal image
    subplot(1,3,1);
    imshow(imgGrey);

%     show new compressed image
    subplot(1,3,2);
    imshow(newImg);

%     show difference
    subplot(1,3,3);
    imshow(subdivided);
end