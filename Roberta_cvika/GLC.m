% GLC matica
function GLC(img, bit, rows, columns, angle, distance)
%     Load and get dimensions from image
    image = imread(img);
    info = imfinfo(img);
    h = info.Height();
    w = info.Width();

    heightSize = h/columns;
    widthSize = w/rows;

%     Image to hsv values
    hsv = rgb2hsv(image);

%     Compression
    img_comp = round(hsv./(256/(2^bit)));
    img_comp = img_comp.*(256/(2^bit));
    
%     s because of subplots
    s = 1;
   
%     zeros matrix
    z = uint8(zeros(h, w, 3));

%     Looping through rows and columns
    for i=1:rows
        for j=1:columns
%             filling z matrix with parts of image
            z = image(heightSize*(i-1)+1:heightSize*i, widthSize*(j-1)+1:widthSize*j, :);
            
%             conversion to gray
            z = rgb2gray(z);
            
%             GLC matrix 
            matica = graycomatrix(z, 'Offset', [angle distance], 'NumLevels',256);
            subplot(rows, columns, s);
            s = s + 1;

%             Get contrast from GLC matrix
            stats = graycoprops(matica, 'Contrast');
            
%             Show GLC matrix
            imshow(matica);

%             Add title to each matrix with contrast values
            title("Contrast: " + stats.Contrast());
        end
    end
end