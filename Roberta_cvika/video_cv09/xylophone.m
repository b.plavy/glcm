%play, pause, end, start, etc.

function varargout = xylophone(varargin)
% XYLOPHONE MATLAB code for xylophone.fig
%      XYLOPHONE, by itself, creates a new XYLOPHONE or raises the existing
%      singleton*.
%
%      H = XYLOPHONE returns the handle to a new XYLOPHONE or the handle to
%      the existing singleton*.
%
%      XYLOPHONE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in XYLOPHONE.M with the given input arguments.
%
%      XYLOPHONE('Property','Value',...) creates a new XYLOPHONE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before xylophone_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to xylophone_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help xylophone

% Last Modified by GUIDE v2.5 15-Nov-2022 12:52:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @xylophone_OpeningFcn, ...
                   'gui_OutputFcn',  @xylophone_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before xylophone is made visible.
function xylophone_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no img args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to xylophone (see VARARGIN)

% Choose default command line img for xylophone
handles.output = hObject;
handles.pause = 0;
handles.edge = 'Canny';

axes(handles.img);
imshow(ones(1));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes xylophone wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = xylophone_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning img args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line img from handles structure
varargout{1} = handles.img;


% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function experiments_Callback(hObject, eventdata, handles)
% hObject    handle to experiments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in playButton.
function playButton_Callback(hObject, eventdata, handles)
% hObject    handle to playButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

while hasFrame(handles.video)
    frame = readFrame(handles.video);
    imshow(frame);
    pause(1/handles.video.FrameRate);
    drawEdge(frame, hObject, eventdata, handles);
    handles = guidata(hObject);

    if handles.pause == 1 
        handles.pause = 0;
        guidata(hObject, handles);
        break;
    end

end

guidata(hObject, handles);

% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
% hObject    handle to about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
candys = 'Bruh';
msgbox(candys);


% --------------------------------------------------------------------
function loadImage_Callback(hObject, eventdata, handles)
% hObject    handle to loadImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname, filterIndex] = uigetfile({'*.mp4'; '*.avi'; '*.png'; '*.bmp'; '*.jpg'}, 'Select image');

if filterIndex == 1 || filterIndex == 2
    video = VideoReader(strcat(pathname, filename));
    handles.video = video;

    vidFrame = readFrame(handles.video);
    imshow(vidFrame);
    drawEdge(vidFrame, hObject, eventdata, handles);
else
    image = imread(strcat(pathname, filename));
    handles.image = image;
    
    axes(handles.img);
    imshow(image);
end



guidata(hObject, handles);

% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

answer = questdlg('Candys?', 'Question', 'Yes', 'No', 'No');

if answer == "Yes"
    close();
end


% --- Executes on button press in prevFrame.
function prevFrame_Callback(hObject, eventdata, handles)
% hObject    handle to prevFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
time = handles.video.CurrentTime;
frame = round(time/(1/handles.video.FrameRate)) - 1;

if frame > 0
    vidFrame = read(handles.video, frame);
    imshow(vidFrame);
    drawEdge(vidFrame, hObject, eventdata, handles);
end

guidata(hObject, handles);


% --- Executes on button press in nextFrame.
function nextFrame_Callback(hObject, eventdata, handles)
% hObject    handle to nextFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if hasFrame(handles.video)
    vidFrame = readFrame(handles.video);
    imshow(vidFrame);
    drawEdge(vidFrame, hObject, eventdata, handles);
end

guidata(hObject, handles);


% --- Executes on button press in endButton.
function endButton_Callback(hObject, eventdata, handles)
% hObject    handle to endButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)1
vidFrame = read(handles.video, Inf);
imshow(vidFrame);
drawEdge(vidFrame, hObject, eventdata, handles);

guidata(hObject, handles);


% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vidFrame = read(handles.video, 1);
imshow(vidFrame);
drawEdge(vidFrame, hObject, eventdata, handles);

guidata(hObject, handles);


% --- Executes on button press in pauseButton.
function pauseButton_Callback(hObject, eventdata, handles)
% hObject    handle to pauseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.pause = 1;
guidata(hObject, handles);

function drawEdge(imag, hObject, eventdata, handles)
axes(handles.imgOutputEdge);
gray = rgb2gray(imag);
edgeImg = edge(gray, handles.edge);
imshow(edgeImg);
axes(handles.img);

% --------------------------------------------------------------------
function roberts_Callback(hObject, eventdata, handles)
% hObject    handle to roberts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.edge = 'Roberts';
guidata(hObject, handles);

time = handles.video.CurrentTime;
frame = round(time/(1/handles.video.FrameRate));

if frame > 0
    vidFrame = read(handles.video, frame);
    imshow(vidFrame);
    drawEdge(vidFrame, hObject, eventdata, handles);
end

guidata(hObject, handles);

% --------------------------------------------------------------------
function canny_Callback(hObject, eventdata, handles)
% hObject    handle to canny (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.edge = 'Canny';
guidata(hObject, handles);

time = handles.video.CurrentTime;
frame = round(time/(1/handles.video.FrameRate));

if frame > 0
    vidFrame = read(handles.video, frame);
    imshow(vidFrame);
    drawEdge(vidFrame, hObject, eventdata, handles);
end

guidata(hObject, handles);
