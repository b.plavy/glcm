% kompresia vyrezov s volitelnym poctom casti po medzerniku
function kompresiaVyrez(input, bit, casti)
    img = imread(input);
    dim = size(img);
    s = 1;

    casti = round(sqrt(casti));
    dupl = img;

    for i = 1:casti      
        for j = 1:casti
            subplot(casti*casti, 2, s*2-1)
            img_output = round(img./(256/(2^bit)));
            img_output = img_output.*(256/(2^bit));
            x1Pos = (i-1)*(dim(1)/casti)+1;
            x2Pos = i*(dim(1)/casti);
            y1Pos = (j-1)*(dim(1)/casti)+1;
            y2Pos = j*(dim(1)/casti);
            imshow(img_output(x1Pos:x2Pos, y1Pos:y2Pos ,:))
            subplot(casti*casti, 2, s*2)

            % histogram
            %imhist(img_output(x1Pos:x2Pos, y1Pos:y2Pos, :));

            % Farebny histogram
%             [yRed, ~] = imhist(img_output(x1Pos:x2Pos, y1Pos:y2Pos, 1));
%             [yGreen, ~] = imhist(img_output(x1Pos:x2Pos, y1Pos:y2Pos, 2));
%             [yBlue, x] = imhist(img_output(x1Pos:x2Pos, y1Pos:y2Pos, 3));
%             %Plot them together in one plot
%             plot(x, yRed, 'Red', x, yGreen, 'Green', x, yBlue, 'Blue');

%           Strukturovany histogram
            hsv=rgb2hsv(img_output(x1Pos:x2Pos, y1Pos:y2Pos ,:));
            imhist(hsv(:, :, 1));
            
            s = s +1;
        end
    end

end