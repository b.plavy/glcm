% BNW, Canny, Roberts

function varargout = untitled(varargin)
% UNTITLED MATLAB code for untitled.fig
%      UNTITLED, by itself, creates a new UNTITLED or raises the existing
%      singleton*.
%
%      H = UNTITLED returns the handle to a new UNTITLED or the handle to
%      the existing singleton*.
%
%      UNTITLED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UNTITLED.M with the given input arguments.
%
%      UNTITLED('Property','Value',...) creates a new UNTITLED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before untitled_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to untitled_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help untitled

% Last Modified by GUIDE v2.5 08-Nov-2022 12:29:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @untitled_OpeningFcn, ...
                   'gui_OutputFcn',  @untitled_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before untitled is made visible.
function untitled_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to untitled (see VARARGIN)

% Choose default command line output for untitled
handles.output = hObject;

axes(handles.Zobrazenie);
imshow(ones([1,1]));

axes(handles.blackAndWhite);
imshow(ones([1,1]));

axes(handles.cannyImage);
imshow(ones([1,1]));

axes(handles.robertsImage);
imshow(ones([1,1]));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes untitled wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = untitled_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in nacitaj.
function nacitaj_Callback(hObject, eventdata, handles)
% hObject    handle to nacitaj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imagepath = uigetfile({'*.png'; '*.bmp'; '*.jpg'}, 'Select image');
handles.imagepath = imagepath;

image = imread(imagepath);
handles.image = image;

axes(handles.Zobrazenie);
imshow(handles.image);
handles.createBnW.Enable = 'on';

guidata(hObject, handles);


% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imagepath = uigetfile({'*.png'; '*.bmp'; '*.jpg'}, 'Select image');
handles.imagepath = imagepath;

image = imread(imagepath);
handles.image = image;

axes(handles.Zobrazenie);
imshow(handles.image);
handles.createBnW.Enable = 'on';

guidata(hObject, handles);


% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Are u sure?', 'Question', 'Yes', 'No', 'No');

if answer == "Yes"
    close();
end

% switch answer
%     case 'No'
% 
%     otherwise
%         close();
% end

% --- Executes on button press in createBnW.
function createBnW_Callback(hObject, eventdata, handles)
% hObject    handle to createBnW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bnwImage = rgb2gray(handles.image);
handles.bnwImage = bnwImage;
    
axes(handles.blackAndWhite);
imshow(handles.bnwImage);
guidata(hObject, handles);


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
% hObject    handle to about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ja = 'Tento super neskutocny matlabovsky skript napisal Ben Dover!';
msgbox(ja);


% --- Executes on slider movement.
function cannySlider_Callback(hObject, eventdata, handles)
% hObject    handle to cannySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
updateCanny(get(handles.cannySlider, 'Value'), hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function cannySlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cannySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function robertsSlider_Callback(hObject, eventdata, handles)
% hObject    handle to robertsSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
updateRoberts(get(handles.robertsSlider, 'Value'), hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function robertsSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robertsSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --------------------------------------------------------------------
function experiment_Callback(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function canny_Callback(hObject, eventdata, handles)
% hObject    handle to canny (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.cannySlider.Enable = 'on';

axes(handles.cannyImage);
imageCanny = edge(handles.bnwImage, 'canny', get(handles.cannySlider, 'Value'));
handles.imageCanny = imageCanny;
imshow(handles.imageCanny);

guidata(hObject, handles);


% --------------------------------------------------------------------
function roberts_Callback(hObject, eventdata, handles)
% hObject    handle to roberts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.robertsSlider.Enable = 'on';

axes(handles.robertsImage);
imageRoberts = edge(handles.bnwImage, 'roberts', get(handles.robertsSlider, 'Value'));
handles.imageRoberts = imageRoberts;
imshow(handles.imageRoberts);


guidata(hObject, handles);



function cannyValue_Callback(hObject, eventdata, handles)
% hObject    handle to cannyValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cannyValue as text
%        str2double(get(hObject,'String')) returns contents of cannyValue as a double

updateCanny(str2num(get(handles.cannyValue, 'String')), hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function cannyValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cannyValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function robertsValue_Callback(hObject, eventdata, handles)
% hObject    handle to robertsValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of robertsValue as text
%        str2double(get(hObject,'String')) returns contents of robertsValue as a double
updateRoberts(str2num(get(handles.robertsValue, 'String')), hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function robertsValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to robertsValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function updateCanny(value, hObject, eventdata, handles)
    handles.cannyValue.String = value;
    set(handles.cannySlider, 'Value', value);
    canny_Callback(hObject, eventdata, handles);


function updateRoberts(value, hObject, eventdata, handles)
    handles.robertsValue.String = value;
    set(handles.robertsSlider, 'Value', value);
    roberts_Callback(hObject, eventdata, handles);
