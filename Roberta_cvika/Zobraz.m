% zobrazenie obrazka a jeho histogramu
function Zobraz(input)
    img = imread(input);

    subplot(1,2,1);
    imshow(img);
    subplot(1,2,2);
    imhist(img);
end