% props graphs from video

function varargout = glcm_video(varargin)
% GLCM_VIDEO MATLAB code for glcm_video.fig
%      GLCM_VIDEO, by itself, creates a new GLCM_VIDEO or raises the existing
%      singleton*.
%
%      H = GLCM_VIDEO returns the handle to a new GLCM_VIDEO or the handle to
%      the existing singleton*.
%
%      GLCM_VIDEO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GLCM_VIDEO.M with the given input arguments.
%
%      GLCM_VIDEO('Property','Value',...) creates a new GLCM_VIDEO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before glcm_video_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to glcm_video_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help glcm_video

% Last Modified by GUIDE v2.5 22-Nov-2022 12:24:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @glcm_video_OpeningFcn, ...
                   'gui_OutputFcn',  @glcm_video_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before glcm_video is made visible.
function glcm_video_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to glcm_video (see VARARGIN)

% Choose default command line output for glcm_video
handles.output = hObject;
handles.pause = 0;
handles.distance = 0;
handles.angle = 1;

handles.z = zeros(256);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes glcm_video wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = glcm_video_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadImage.
function loadImage_Callback(hObject, eventdata, handles)
% hObject    handle to loadImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname, filterIndex] = uigetfile({'*.mp4'; '*.avi'; '*.png'; '*.bmp'; '*.jpg'}, 'Select image');

if filterIndex == 1 || filterIndex == 2
    video = VideoReader(strcat(pathname, filename));
    handles.video = video;

    vidFrame = readFrame(handles.video);

    frames = video.NumFrames;
    handles.contrast = zeros(frames, 1);
    handles.homo = zeros(frames, 1);
    handles.energy = zeros(frames, 1);

    axes(handles.ImagePlane);
    imshow(vidFrame);
else
    image = imread(strcat(pathname, filename));
    handles.image = image;
    axes(handles.ImagePlane);
    imshow(image);
end



guidata(hObject, handles);


% --- Executes on button press in playBtn.
function playBtn_Callback(hObject, eventdata, handles)
% hObject    handle to playBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
count = 0;

while hasFrame(handles.video)
    frame = readFrame(handles.video);
    imshow(frame);

    count = count + 1;
    handles.count = count;

    pause(1/handles.video.FrameRate);

    drawEdge(frame, hObject, eventdata, handles);
    handles = guidata(hObject);

    if handles.pause == 1 
        handles.pause = 0;
        guidata(hObject, handles);
        break;
    end

end

guidata(hObject, handles);


% --- Executes on button press in stopBtn.
function stopBtn_Callback(hObject, eventdata, handles)
% hObject    handle to stopBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.pause = 1;
guidata(hObject, handles);


% --- Executes on button press in glcmBtn.
function glcmBtn_Callback(hObject, eventdata, handles)
% hObject    handle to glcmBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function drawEdge(imag, hObject, eventdata, handles)

axes(handles.contrastGraph);

gray = rgb2gray(imag);
matica = graycomatrix(gray, 'Offset', [handles.angle handles.distance], 'NumLevels',256);
contrastValue = graycoprops(matica, 'Contrast');
handles.contrast(handles.count) = contrastValue.Contrast();
plot(handles.contrast);
title('Contrast');

axes(handles.homogenityGraph);
homoValue = graycoprops(matica, 'Homogeneity');
handles.homo(handles.count) = homoValue.Homogeneity();
plot(handles.homo);
title('Homo');

axes(handles.energyGraph);
energyValue = graycoprops(matica, 'Energy');
handles.energy(handles.count) = energyValue.Energy();
plot(handles.energy);
title('Energy');

axes(handles.ImagePlane);

guidata(hObject, handles);



function angleanddistance_Callback(hObject, eventdata, handles)
% hObject    handle to angleanddistance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of angleanddistance as text
%        str2double(get(hObject,'String')) returns contents of angleanddistance as a double


% --- Executes during object creation, after setting all properties.
function angleanddistance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angleanddistance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function angleValue_Callback(hObject, eventdata, handles)
% hObject    handle to angleValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of angleValue as text
%        str2double(get(hObject,'String')) returns contents of angleValue as a double
handles.angle = str2double(get(handles.angleValue, 'String'));

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function angleValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angleValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function distanceValue_Callback(hObject, eventdata, handles)
% hObject    handle to distanceValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of distanceValue as text
%        str2double(get(hObject,'String')) returns contents of distanceValue as a double
handles.distance = str2double(get(handles.distanceValue, 'String'));

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function distanceValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to distanceValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
