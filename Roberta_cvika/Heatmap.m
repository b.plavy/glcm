% GLC matica + Korelacia cez heatmapu
function Heatmap(img, rows, columns, angle, distance)
%     load and get dimensions from image
    image = imread(img);
    info = imfinfo(img);
    h = info.Height();
    w = info.Width();

    heightSize = h/columns;
    widthSize = w/rows;

%     s cuz subplot
    s = 1;

%     zeros matrixes
    z = uint8(zeros(h, w, 3));

    mCom = zeros(rows, columns);
    mCor = zeros(rows, columns);

%     looping through rows and columns
    for i=1:rows
        for j=1:columns
%             fill zeros matrix with parts of image
            z = image(heightSize*(i-1)+1:heightSize*i, widthSize*(j-1)+1:widthSize*j, :);
            
%             conversion to grey
            z = rgb2gray(z);

%             GLC matrix
            matica = graycomatrix(z, 'Offset', [angle distance], 'NumLevels',256);

            s = s + 1;
            
%             Get correlation from matrix
            stats = graycoprops(matica, {'correlation'});

%             put correlation value into mCor matrix
            mCor(i, j) = stats.Correlation();
        end
    end

%     loop through rows and columns
    for k=1:rows
        for l=1:columns
%             put differences between rows and columns to mCom matrix
           mCom(k, l) = mCor(k,l) - mCor(l,k);
           mCom = abs(mCom);
        end
    end

%     create heatmap
    h = heatmap(mCom);
    disp(h);
end