% custom histogram from image
function Histogram(input)
% load image and convert it to hsv values
img = imread(input);
hsv = rgb2hsv(img);
hsv = uint8(hsv*255);

% create counter with 256 values by 1 column
counter = zeros(256,1);

% loop to 255
for i=0:255
%     set value on counter spot to how many values are in the color
    counter(i+1) = sum(sum(hsv(:,:,3)==i));
end

% subplot with custom histogram
subplot(1,2,1);
stem(counter);

% subplot with matlabs histogram
subplot(1,2,2);
imhist(img);

end