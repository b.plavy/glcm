% random rows and colors with compression
function randomColors(input, rows, bit, r, g, b)
    img = imread(input);

    info = imfinfo(input);
    height = info.Height();
    width = info.Width();

    heightSize = height/rows;

    z = zeros(width, height, 3);

    ra = randperm(rows);

    r1 = [r, g, b];

    if r1(1) == "r"
        if r1(2) == "g"
            r1 = [1,2,3];
        elseif r1(2) == "b"
            r1 = [1,3,2];
        end
    elseif r1(1) == "g"
        if r1(2) == "r"
            r1 = [2,1,3];
        elseif r1(2) == "b"
            r1 = [2,3,1];
        end
    elseif r1(1) == "b"
        if r1(2) == "r"
            r1 = [3,1,2];
        elseif r1(2) == "g"
            r1 = [3,2,1];
        end
    end

    img_output = round(img./(256/(2^bit)));
    img_output = img_output.*(256/(2^bit));

    for i=1:rows
        for j=1:3
            z(heightSize*(i-1)+1:heightSize*(i-1)+heightSize, : , r1(j)) = img_output(heightSize*(ra(i)-1)+1:heightSize*(ra(i)-1)+heightSize, : , j);
        end
    end

    imshow(uint8(z))

end