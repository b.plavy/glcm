function varargout = Zapocet(varargin)
% ZAPOCET MATLAB code for Zapocet.fig
%      ZAPOCET, by itself, creates a new ZAPOCET or raises the existing
%      singleton*.
%
%      H = ZAPOCET returns the handle to a new ZAPOCET or the handle to
%      the existing singleton*.
%
%      ZAPOCET('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZAPOCET.M with the given input arguments.
%
%      ZAPOCET('Property','Value',...) creates a new ZAPOCET or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Zapocet_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Zapocet_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Zapocet

% Last Modified by GUIDE v2.5 07-Dec-2022 12:40:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Zapocet_OpeningFcn, ...
                   'gui_OutputFcn',  @Zapocet_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Zapocet is made visible.
function Zapocet_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Zapocet (see VARARGIN)

% Choose default command line output for Zapocet
handles.output = hObject;
handles.path = 0;
handles.img1 = 0;
handles.img2 = 0;
handles.img3 = 0;
handles.img4 = 0;
handles.img5 = 0;

handles.sortimg1 = 0;
handles.sortimg2 = 0;
handles.sortimg3 = 0;
handles.sortimg4 = 0;
handles.sortimg5 = 0;

handles.currentmid = 3;
handles.min = 3;
handles.max = 0;

imshow(ones(1), 'Parent', handles.ax1)
imshow(ones(1), 'Parent', handles.ax2)
imshow(ones(1), 'Parent', handles.ax3)
imshow(ones(1), 'Parent', handles.ax4)
imshow(ones(1), 'Parent', handles.ax5)

handles.value = 1;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Zapocet wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Zapocet_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.current+2 < handles.max-handles.min)
    handles.current = handles.current+1;
    redraw(hObject, eventdata, handles)
end
guidata(hObject, handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.current-2 > handles.min-2)
    handles.current = handles.current-1;
    redraw(hObject, eventdata, handles)
end
guidata(hObject, handles);

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
value = get(hObject, "Value");
handles.value = value;
if(value == 1)
[x,idx]=sort([handles.S.energyR]);
handles.idx = idx;
handles.x = x;
elseif(value == 2)
[x,idx]=sort([handles.S.energyG]);
handles.idx = idx;
handles.x = x;

elseif(value == 3)
[x,idx]=sort([handles.S.energyB]);
handles.idx = idx;
handles.x = x;

elseif(value == 4)
[x,idx]=sort([handles.S.contrast]);
handles.idx = idx;
handles.x = x;

elseif(value == 5)
[x,idx]=sort([handles.S.homogenity]);
handles.idx = idx;
handles.x = x;
end

current = 3;
guidata(hObject, handles);
redraw(hObject, eventdata, handles);

function redraw(hObject, eventdata, handles)
imshow(handles.S(handles.idx(handles.current-2)).Image, 'Parent', handles.ax1);
imshow(handles.S(handles.idx(handles.current-1)).Image, 'Parent', handles.ax2);
imshow(handles.S(handles.idx(handles.current)).Image, 'Parent', handles.ax3);
imshow(handles.S(handles.idx(handles.current+1)).Image, 'Parent', handles.ax4);
imshow(handles.S(handles.idx(handles.current+2)).Image, 'Parent', handles.ax5);

value = handles.value;

if(value == 1)
set(handles.text2, 'String', handles.S(handles.idx(handles.current-2)).energyR);
set(handles.text3, 'String', handles.S(handles.idx(handles.current-1)).energyR);
set(handles.text4, 'String', handles.S(handles.idx(handles.current)).energyR);
set(handles.text5, 'String', handles.S(handles.idx(handles.current+1)).energyR);
set(handles.text6, 'String', handles.S(handles.idx(handles.current+2)).energyR);

elseif(value == 2)
set(handles.text2, 'String', handles.S(handles.idx(handles.current-2)).energyG);
set(handles.text3, 'String', handles.S(handles.idx(handles.current-1)).energyG);
set(handles.text4, 'String', handles.S(handles.idx(handles.current)).energyG);
set(handles.text5, 'String', handles.S(handles.idx(handles.current+1)).energyG);
set(handles.text6, 'String', handles.S(handles.idx(handles.current+2)).energyG);
elseif(value == 3)
set(handles.text2, 'String', handles.S(handles.idx(handles.current-2)).energyB);
set(handles.text3, 'String', handles.S(handles.idx(handles.current-1)).energyB);
set(handles.text4, 'String', handles.S(handles.idx(handles.current)).energyB);
set(handles.text5, 'String', handles.S(handles.idx(handles.current+1)).energyB);
set(handles.text6, 'String', handles.S(handles.idx(handles.current+2)).energyB);
elseif(value == 4)
set(handles.text2, 'String', handles.S(handles.idx(handles.current-2)).contrast);
set(handles.text3, 'String', handles.S(handles.idx(handles.current-1)).contrast);
set(handles.text4, 'String', handles.S(handles.idx(handles.current)).contrast);
set(handles.text5, 'String', handles.S(handles.idx(handles.current+1)).contrast);
set(handles.text6, 'String', handles.S(handles.idx(handles.current+2)).contrast);
elseif(value == 5)
set(handles.text2, 'String', handles.S(handles.idx(handles.current-2)).homogenity);
set(handles.text3, 'String', handles.S(handles.idx(handles.current-1)).homogenity);
set(handles.text4, 'String', handles.S(handles.idx(handles.current)).homogenity);
set(handles.text5, 'String', handles.S(handles.idx(handles.current+1)).homogenity);
set(handles.text6, 'String', handles.S(handles.idx(handles.current+2)).homogenity);
end

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.path = uigetdir();
handles.files = dir(handles.path);
handles.max = length(handles.files);
handles.current = 3;

handles.S(handles.min:handles.max-1) = struct('Image', [], 'Label', '', 'Index', [], 'energyR', [], 'energyG', [], 'energyB', [], 'contrast', [], 'homogenity', []);
for j = handles.min:handles.max-1
  handles.S(j-handles.min+1).Image = imread(strcat(handles.path,"\",handles.files(j).name));
  handles.S(j-handles.min+1).Label = handles.files(j).name;
  handles.S(j-handles.min+1).Index = j;  
end

for j = 1:handles.max-handles.min
    im = handles.S(j).Image;
    handles.S(j).energyR = sum(sum(im(:,:,1)))/(size(im,1)*size(im,2));
end
for j = 1:handles.max-handles.min
    im = handles.S(j).Image;
    handles.S(j).energyG = sum(sum(im(:,:,2)))/(size(im,1)*size(im,2));
end
for j = 1:handles.max-handles.min
    im = handles.S(j).Image;
    handles.S(j).energyB = sum(sum(im(:,:,3)))/(size(im,1)*size(im,2));
end
for j = 1:handles.max-handles.min
    im = im2gray(handles.S(j).Image);
    glcm = graycomatrix(im,'Offset',[0 1]);
    con = graycoprops(glcm,'Contrast');
    handles.S(j).contrast = con.Contrast;
end
for j = 1:handles.max-handles.min
    im = im2gray(handles.S(j).Image);
    glcm = graycomatrix(im,'Offset',[0 1]);
    con = graycoprops(glcm,'Homogeneity');
    handles.S(j).homogenity = con.Homogeneity;
end

[x,idx]=sort([handles.S.energyR])
handles.idx = idx;

guidata(hObject, handles);
redraw(hObject, eventdata, handles);

% --------------------------------------------------------------------
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
